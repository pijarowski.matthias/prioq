import java.util.ArrayList;

// Als Binomial-Halde implementierte Minimum-Vorrangwarteschlange
// mit Prioritäten eines beliebigen Typs P (der die Schnittstelle
// Comparable<P> oder Comparable<P'> für einen Obertyp P' von P
// implementieren muss) und zusätzlichen Daten eines beliebigen Typs D.
class BinHeap <P extends Comparable<? super P>, D> {
    // Eintrag einer solchen Warteschlange bzw. Halde, bestehend aus
    // einer Priorität prio mit Typ P und zusätzlichen Daten data mit
    // Typ D.
    // Wenn der Eintrag momentan tatsächlich zu einer Halde gehört,
    // verweist node auf den zugehörigen Knoten eines Binomialbaums
    // dieser Halde.
    public static class Entry <P, D> {
        // Priorität, zusätzliche Daten und zugehöriger Knoten.
        private P prio;
        private D data;
        private Node<P, D> node;

        // Eintrag mit Priorität p und zusätzlichen Daten d erzeugen.
        private Entry (P p, D d) {
            prio = p;
            data = d;
        }

        // Priorität bzw. zusätzliche Daten liefern.
        public P prio () { return prio; }
        public D data () { return data; }
    }

    // Knoten eines Binomialbaums innerhalb einer solchen Halde.
    // Neben den eigentlichen Knotendaten (degree, parent, child,
    // sibling), enthält der Knoten einen Verweis auf den zugehörigen
    // Eintrag.
    private static class Node <P, D> {
        // Zugehöriger Eintrag.
        private Entry<P, D> entry;

        // Grad des Knotens.
        private int degree;

        // Vorgänger (falls vorhanden; bei einem Wurzelknoten null).
        private Node<P, D> parent;

        // Nachfolger mit dem größten Grad
        // (falls vorhanden; bei einem Blattknoten null).
        private Node<P, D> child;

        // Zirkuläre Verkettung aller Nachfolger eines Knotens
        // bzw. einfache Verkettung aller Wurzelknoten einer Halde,
        // jeweils sortiert nach aufsteigendem Grad.
        private Node<P, D> sibling;

        // Knoten erzeugen, der auf den Eintrag e verweist
        // und umgekehrt.
        private Node (Entry<P, D> e) {
            entry = e;
            e.node = this;
        }

        // Priorität des Knotens, d. h. des zugehörigen Eintrags
        // liefern.
        private P prio () { return entry.prio; }
    }

    private Node<P, D> entryNode;

    public BinHeap() {
    }

    public boolean isEmpty() {
        return entryNode == null;
    }

    public int size() {
        Node<P, D> curNode = this.entryNode;
        if (curNode == null)
            return 0;
        int size = 0;
        while (curNode != null) {
            size += 1 << curNode.degree;
            curNode = curNode.sibling;
        }
        return size;
    }

    public boolean contains(Entry<P, D> e) {
        //Wenn e.prio == null dann muss das element entfernt worden sein
        if (e == null || this.isEmpty() || e.prio() == null)
            return false;

        Node<P, D> rootNode = e.node;
        while (rootNode.parent != null) {
            rootNode = rootNode.parent;
        }
        Node<P, D> curNode = this.entryNode;
        while (curNode != null) {
            if (curNode.equals(rootNode))
                return true;
            curNode = curNode.sibling;
        }
        return false;
    }

    public Entry<P, D> insert(P p, D d) {
        if (p == null)
            return null;

        BinHeap<P, D> tempheap = new BinHeap<>();
        Entry<P, D> entry = new Entry<>(p, d);
        tempheap.entryNode = new Node<>(entry);
        this.mergeHeap(tempheap);

        return entry;
    }

    public boolean changePrio (Entry<P, D> e, P p) {
        if (e == null || p == null || !this.contains(e))
            return false;

        if (p.compareTo(e.prio()) <= 0) {
            e.prio = p;

            while (e.node.parent != null && e.prio.compareTo(e.node.parent.prio()) < 0) {
                e.node.entry = e.node.parent.entry;
                e.node.parent.entry = e;
                e.node.entry.node = e.node;
                e.node = e.node.parent;
            }
        }
        else if (e.node.child != null){
            this.remove(e);
            this.insert(p, e.data());
        }
        else {
            e.prio = p;
        }

        return true;
    }

    public Entry<P, D> minimum() {
        if (this.isEmpty())
            return null;

        Entry<P, D> min = entryNode.entry;
        if (min.prio() == null)
            return min;

        Node<P, D> curNode = entryNode.sibling;

        while (curNode != null) {
            if (curNode.prio() == null)
                return curNode.entry;
            if (curNode.prio().compareTo(min.prio()) < 0)
                min = curNode.entry;
            curNode = curNode.sibling;
        }

        return min;
    }

    public Entry<P, D> extractMin() {
        if (this.isEmpty())
            return null;

        Node<P, D> min = this.minimum().node;

        if (min.equals(entryNode)) {
            // Der Fall wenn das minimum die erste Stelle Wurzelknoten ist
            this.entryNode = min.sibling;
        }
        else {
            Node<P, D> curNode = this.entryNode;
            while (curNode != null) {
                if (curNode.sibling.equals(min) && min.sibling == null) {
                    // Der Fall wenn das minimum die letzte Stelle Wurzelknoten ist
                    curNode.sibling = null;
                    break;
                }
                if (curNode.sibling.equals(min) && min.sibling != null) {
                    // Der Fall wenn das minimum in der Mitte der Wurzelknoten ist
                    curNode.sibling = min.sibling;
                    break;
                }
                curNode = curNode.sibling;
            }
        }

        if (min.child != null) {
            // min node hat kind
            if (min.child.equals(min.child.sibling)) {
                // Hat nur ein Kind
                Node<P, D> child = min.child;
                child.sibling = child.parent = null;
                BinHeap<P, D> temp = new BinHeap<>();
                temp.entryNode = child;
                this.mergeHeap(temp);
            }
            else {
                // Hat mehrere Kinder
                Node<P, D> childHighestDeg = min.child;
                while (true) {
                    Node<P, D> nextNode = childHighestDeg.sibling;
                    childHighestDeg.sibling = nextNode.sibling;

                    nextNode.sibling = null;
                    nextNode.parent = null;

                    BinHeap<P, D> tempHeap = new BinHeap<>();
                    tempHeap.entryNode = nextNode;
                    this.mergeHeap(tempHeap);

                    if (nextNode.equals(childHighestDeg))
                        break;
                }
            }
        }

//        if (min.child != null) {
//            ArrayList<Node<P, D>> childNodes = new ArrayList<>();
//            Node<P, D> curNode = min.child;
//            while (true) {
//                childNodes.add(curNode);
//
//                if (curNode.sibling.equals(curNode))
//                    break;
//
//                Node<P, D> nextNode = curNode.sibling;
//                while (true) {
//                    if (nextNode.sibling.equals(curNode)) {
//                        nextNode.sibling = curNode.sibling;
//                        break;
//                    }
//                    nextNode = nextNode.sibling;
//                }
//
//                curNode = nextNode;
//            }
//            for (Node<P, D> childNode: childNodes){
//                childNode.sibling = null;
////                childNode.parent = null;
//                BinHeap<P, D> tempHeap = new BinHeap<>();
//                tempHeap.entryNode = childNode;
//                this.mergeHeap(tempHeap);
//            }
//        }

        return min.entry;
    }

    public boolean remove(Entry<P, D> e) {
        if (e == null || !this.contains(e))
            return false;

        e.prio = null;

        while (e.node.parent != null) {
            e.node.entry = e.node.parent.entry;
            e.node.parent.entry = e;
            e.node.entry.node = e.node;
            e.node = e.node.parent;
        }

        this.extractMin();

        return true;
    }

    public void dump() {
        Node<P, D> node = this.entryNode;
        while (node != null) {
            System.out.println(node.prio() + " " + node.entry.data());

            if (node.child != null)
                printSubTree(node.child.sibling, 1);

            node = node.sibling;
        }
    }

    private void printSubTree(Node<P, D> startNode, int depth) {
        if (startNode == null)
            return;
        String spaces = "";
        for (int i = 0; i < depth; i++) {
            spaces = spaces.concat("  ");
        }
        System.out.println(spaces + startNode.prio() + " " + startNode.entry.data());
        if (startNode.child == null && startNode.equals(startNode.parent.child))
            return;
        // für jedes kind in der list wieder printSubTree aufrufen
        if (startNode.child != null)
            this.printSubTree(startNode.child.sibling, depth + 1);
        if (!startNode.equals(startNode.parent.child))
            this.printSubTree(startNode.sibling, depth);

    }

    private Node<P, D> mergeTree(Node<P, D> b1, Node<P, D> b2) {
        if (b1.prio().compareTo(b2.prio()) > 0) {
            //b1 prio > b2 prio -> mache b1 zum Kind von b2
            b2.sibling = null;
            b2.degree++;
            b1.parent = b2;
            if (b2.child == null) {
                b2.child = b1.sibling = b1;
            }
            else {
                b1.sibling = b2.child.sibling;
                b2.child = b2.child.sibling = b1;
            }
            return b2;
        }
        else {
            b1.sibling = null;
            b1.degree++;
            b2.parent = b1;
            if (b1.child == null) {
                b1.child = b2.sibling = b2;
            }
            else {
                b2.sibling = b1.child.sibling;
                b1.child = b1.child.sibling = b2;
            }
            return b1;
        }
    }

    private void mergeHeap(BinHeap<P, D> h2) {
        BinHeap<P, D> hFinal = new BinHeap<>();
        ArrayList<Node<P, D>> temp = new ArrayList<>();
        int k = 0;

        while (!this.isEmpty() || !h2.isEmpty() || !temp.isEmpty()) {
            if (this.entryNode != null && this.entryNode.degree == k) {
                temp.add(this.entryNode);
                this.entryNode = this.entryNode.sibling;
            }
            if (h2.entryNode != null && h2.entryNode.degree == k) {
                temp.add(h2.entryNode);
                h2.entryNode = h2.entryNode.sibling;
            }
            if (temp.size() % 2 == 1) {
                Node<P, D> node = temp.remove(temp.size() - 1);
                if (hFinal.isEmpty()) {
                    hFinal.entryNode = node;
                    hFinal.entryNode.parent = null;
                    hFinal.entryNode.sibling = null;
                }
                else {
                    Node<P, D> curNode = hFinal.entryNode;
                    while (curNode.sibling != null) {
                        curNode = curNode.sibling;
                    }
                    node.sibling = null;
                    node.parent = null;
                    curNode.sibling = node;
                }
            }
            if (temp.size() == 2) {
                Node<P, D> node = this.mergeTree(temp.get(0), temp.get(1));
                temp.clear();
                temp.add(node);
            }
            k++;
        }
        this.entryNode = hFinal.entryNode;
    }

    public static void main(String[] args) {
        BinHeap<String, Integer> binHeap = new BinHeap<>();
//        Entry<String, Integer> a = binHeap.insert("a", 0);
//        binHeap.insert("b", 1);
//        binHeap.insert("c", 2);
//        binHeap.insert("d", 3);
//        binHeap.insert("e", 4);
//        binHeap.insert("f", 5);
//        binHeap.insert("g", 6);
//        Entry<String, Integer> h = binHeap.insert("h", 7);
//        binHeap.insert("i", 8);
//        Entry<String, Integer> j = binHeap.insert("j", 9);
//        binHeap.insert("k", 10);

        for (int i = 0; i < 24; i++) {
            binHeap.insert(Character.toString((char)i+97), i);
        }
        binHeap.dump();
    }
}